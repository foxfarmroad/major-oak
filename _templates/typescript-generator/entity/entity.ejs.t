---
to: src/entity/<%= h.capitalize(name) %>.ts
---
export interface <%= h.capitalize(name) %> {
<%
Object.keys(locals)
  .filter(key => key === 'properties')
  .map(key => locals[key])
  .map(value => value.split(','))
  .reduce((acc, cur) => {
    cur.forEach(value => acc.push(value))
    return acc
  }, [])
  .map(value => value.trim())
  .forEach(value => {
-%>
  readonly <%= value %>: <%= eval(value + '.type') -%>;
<%
})
-%>
}