module.exports = {
  prompt: ({ inquirer }) => {
    // defining questions in arrays ensures all questions are asked before next prompt is executed
    const questions = [
      {
        type: 'input',
        name: 'name',
        message: 'Name of interface? (ex: Foo)',
      },
      {
        type: 'input',
        name: 'properties',
        message: 'Property names? (separate by comma: "property1, property2, property3")',
      }
    ]

    return inquirer
      .prompt(questions)
      .then(answers => {
        const { properties } = answers
        const questions = []
        // these values can be retrieved in the template with: eval(property + '.type')
        properties.split(',').forEach((property) => {
          questions.push({
            type: 'input',
            name: property.trim() + '.type',
            message: `Input the type for ${property.trim()} (ex: boolean)`,
          })
        })
        // both set of answers must be returned as a merged object, else the previous set of answers won't be available to the templates
        return inquirer.prompt(questions).then(nextAnswers => Object.assign({}, answers, nextAnswers))
      })
  },
}