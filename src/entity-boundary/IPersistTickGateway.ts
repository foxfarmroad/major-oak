import { IPersistGateway } from "./IPersistGateway";
import { ITick } from "./ITick";

export interface IPersistTickGateway extends IPersistGateway {
  persist(tick: ITick): Promise<ITick>;
}
