import { IStringAny } from "./IStringAny";

export interface IQuote extends IStringAny {
  readonly askPrice: string;
  readonly askSize: number;
  readonly bidPrice: string;
  readonly bidSize: string;
  readonly lastTradePrice: string;
  readonly lastExtendedHoursTradePrice: string;
  readonly previousClose: string;
  readonly adjustedPreviousClose: string;
  readonly previousCloseDate: string;
  readonly symbol: string;
  readonly tradingHalted: boolean;
  readonly hasTraded: boolean;
  readonly lastTradePriceSource: string;
  readonly updatedAt: string;
  readonly instrument: string;
  // {
  //   "ask_price":"92.480000",
  //   "ask_size":100,
  //   "bid_price":"92.410000",
  //   "bid_size":400,
  //   "last_trade_price":"92.470000",
  //   "last_extended_hours_trade_price":"92.470000",
  //   "previous_close":"90.690000",
  //   "adjusted_previous_close":"90.690000",
  //   "previous_close_date":"2018-09-21",
  //   "symbol":"TEAM",
  //   "trading_halted":false,
  //   "has_traded":true,
  //   "last_trade_price_source":"consolidated",
  //   "updated_at":"2018-09-24T20:54:14Z",
  //   "instrument":"https://api.robinhood.com/instruments/bc06e768-0955-49f2-ab45-2e07cab0fbe2/"
  // }
}
