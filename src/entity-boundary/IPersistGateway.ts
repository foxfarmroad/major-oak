import { IStringAny } from "./IStringAny";

export interface IPersistGateway {
  persist(tick: IStringAny): Promise<IStringAny>;
}
