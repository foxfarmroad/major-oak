import { IStringAny } from "./IStringAny";

export interface ITick extends IStringAny {
  readonly symbol: string;
  readonly timestamp: number;
  readonly price: number;
  readonly volume: number;
}
