import { IFetchGateway } from "./IFetchGateway";
import { IPersistGateway } from "./IPersistGateway";
import { IQuote } from "./IQuote";
import { ITick } from "./ITick";
import { ITickerSymbol } from "./ITickerSymbol";

export interface IFetchQuoteGateway extends IFetchGateway {
  fetch(symbol: ITickerSymbol): Promise<IQuote>;
}
