import { IStringAny } from "./IStringAny";

export interface ITickerSymbol extends IStringAny {
  readonly symbol: string;
}
