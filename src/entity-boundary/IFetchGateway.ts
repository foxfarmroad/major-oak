import { TickerSymbol } from "./../entity/TickerSymbol";
import { IQuote } from "./IQuote";
import { IStringAny } from "./IStringAny";

export interface IFetchGateway {
  fetch(symbol: IStringAny): Promise<IStringAny>;
}
