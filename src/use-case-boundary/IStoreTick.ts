import { ITick } from "../entity-boundary/ITick";

export interface IStoreTick {
  persist(tick: ITick): Promise<ITick>;
}
