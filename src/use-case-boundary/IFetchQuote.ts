import { IQuote } from "../entity-boundary/IQuote";
import { TickerSymbol } from "./../entity/TickerSymbol";

export interface IFetchQuote {
  fetch(tickerSymbol: TickerSymbol): Promise<IQuote>;
}
