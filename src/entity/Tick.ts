import { IStringAny } from "./../entity-boundary/IStringAny";
import { ITick } from "./../entity-boundary/ITick";

export class Tick implements ITick {
  constructor(readonly symbol: string, readonly timestamp: number, readonly price: number, readonly volume: number) {}
}
