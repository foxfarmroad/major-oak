import { ITickerSymbol } from "./../entity-boundary/ITickerSymbol";

export class TickerSymbol implements ITickerSymbol {
  constructor(readonly symbol: string) {}
}
