const TYPE = {
  DataMapper: Symbol.for("DataMapper"),
  IDataMapper: Symbol.for("IDataMapper"),
  IFetchGateway: Symbol.for("IFetchGateway"),
  IFetchQuoteGateway: Symbol.for("IFetchQuoteGateway"),
  IPersistGateway: Symbol.for("IPersistGateway"),
  IPersistTickGateway: Symbol.for("IPersistTickGateway"),
  IQuoteData: Symbol.for("IQuoteData"),
  ISaveTick: Symbol.for("ISaveTick"),
  IStringAny: Symbol.for("IStringAny"),
  ITick: Symbol.for("ITick"),
  Logger: Symbol.for("Logger"),
  Region: Symbol.for("Region"),
  RobinhoodNode: Symbol.for("RobinhoodNode"),
  Stock: Symbol.for("Stock"),
};

export { TYPE };
