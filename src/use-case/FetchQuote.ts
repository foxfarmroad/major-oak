import { IFetchQuoteGateway } from "./../entity-boundary/IFetchQuoteGateway";
import { IQuote } from "./../entity-boundary/IQuote";
import { ITickerSymbol } from "./../entity-boundary/ITickerSymbol";
import { IFetchQuote } from "./../use-case-boundary/IFetchQuote";

import { inject, injectable } from "inversify";

import { TYPE } from "../types/types";

@injectable()
export class FetchQuote implements IFetchQuote {
  @inject(TYPE.IFetchQuoteGateway)
  private fetchQuoteGateway: IFetchQuoteGateway;

  fetch(tickerSymbol: ITickerSymbol): Promise<IQuote> {
    return this.fetchQuoteGateway.fetch(tickerSymbol);
  }
}
