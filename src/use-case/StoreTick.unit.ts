import { Substitute } from "@fluffy-spoon/substitute";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinonChai from "sinon-chai";
import { ITick } from "../entity-boundary/ITick";
import { IStoreTick } from "../use-case-boundary/IStoreTick";

chai.use(sinonChai);
chai.use(chaiAsPromised);

const expect = chai.expect;
const assert = chai.assert;

import { Container } from "inversify";
import "reflect-metadata";
import { DynamoDBContainer } from "../adapter/dynamodb/dynamodb.config";
import { IPersistTickGateway } from "../entity-boundary/IPersistTickGateway";
import { TYPE } from "../types/types";
import { StoreTick } from "./StoreTick";
import { UseCaseContainer } from "./use-case.config";

class TestTick implements ITick {
  readonly symbol = "TEST";
  readonly timestamp = 0;
  readonly price: 1;
  readonly volume: 2;
}

describe("SaveTick", () => {
  let testTick;

  const storeTickContainer = new Container();
  storeTickContainer.bind<IStoreTick>(TYPE.ISaveTick).to(StoreTick);

  const mergedContainer = Container.merge(DynamoDBContainer, UseCaseContainer);
  const testContainer = Container.merge(mergedContainer, storeTickContainer);

  beforeEach(() => {
    testContainer.snapshot();
    testTick = new TestTick();
  });

  afterEach(() => {
    testContainer.restore();
  });

  it("can persist", async () => {
    testContainer.unbind(TYPE.IPersistTickGateway);
    const mockedPersistTickGateway = Substitute.for<IPersistTickGateway>();
    mockedPersistTickGateway.persist(testTick).returns(Promise.resolve(testTick));
    testContainer.bind<IPersistTickGateway>(TYPE.IPersistTickGateway).toConstantValue(mockedPersistTickGateway);

    const storeTick: IStoreTick = testContainer.get<IStoreTick>(TYPE.ISaveTick);
    const result = await storeTick.persist(testTick);
    assert.deepEqual(result, testTick);
    mockedPersistTickGateway.received(1).persist(testTick);
  });

  it("can error", async () => {
    testContainer.unbind(TYPE.IPersistTickGateway);
    const mockedPersistTickGateway = Substitute.for<IPersistTickGateway>();
    mockedPersistTickGateway.persist(testTick).returns(Promise.reject(new Error()));
    testContainer.bind<IPersistTickGateway>(TYPE.IPersistTickGateway).toConstantValue(mockedPersistTickGateway);

    const storeTick: IStoreTick = testContainer.get<IStoreTick>(TYPE.ISaveTick);
    assert.isRejected(storeTick.persist(testTick));
    mockedPersistTickGateway.received(1).persist(testTick);
  });
});
