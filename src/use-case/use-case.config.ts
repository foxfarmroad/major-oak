import { Container } from "inversify";
import { DynamoDBPersistTickGateway } from "../adapter/dynamodb/DynamoDBPersistTickGateway";
import { RobinhoodFetchQuoteGateway } from "../adapter/robinhood/RobinhoodFetchQuoteGateway";
import { IPersistTickGateway } from "../entity-boundary/IPersistTickGateway";
import { TYPE } from "../types/types";
import { IFetchQuoteGateway } from "./../entity-boundary/IFetchQuoteGateway";

const UseCaseContainer = new Container();
UseCaseContainer.bind<IPersistTickGateway>(TYPE.IPersistTickGateway).to(DynamoDBPersistTickGateway);
UseCaseContainer.bind<IFetchQuoteGateway>(TYPE.IFetchQuoteGateway).to(RobinhoodFetchQuoteGateway);

export { UseCaseContainer };
