import { IStoreTick } from "../use-case-boundary/IStoreTick";

import { inject, injectable } from "inversify";

import { IPersistTickGateway } from "../entity-boundary/IPersistTickGateway";
import { ITick } from "../entity-boundary/ITick";
import { TYPE } from "../types/types";

@injectable()
export class StoreTick implements IStoreTick {

  @inject(TYPE.IPersistTickGateway)
  private persistTickGateway: IPersistTickGateway;

  persist(tick: ITick): Promise<ITick> {
    return this.persistTickGateway.persist(tick);
  }
}
