import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinonChai from "sinon-chai";
import { IPersistTickGateway } from "./../entity-boundary/IPersistTickGateway";

chai.use(sinonChai);
chai.use(chaiAsPromised);

const expect = chai.expect;

import "reflect-metadata";
import { DynamoDBContainer } from "../adapter/dynamodb/dynamodb.config";
import { RobinhoodContainer } from "../adapter/robinhood/robinhood.config";
import { TYPE } from "../types/types";
import { UseCaseContainer } from "./use-case.config";

import { inject, injectable } from "inversify";
import { Container } from "inversify";

@injectable()
class TestClass {
  @inject(TYPE.IPersistTickGateway)
  readonly persistTickGateway: IPersistTickGateway;
}

describe("Use case config", () => {
  it("injects dependencies", () => {
    const dependencyContainer = Container.merge(DynamoDBContainer, RobinhoodContainer);
    const testClass: TestClass = Container.merge(dependencyContainer, UseCaseContainer).resolve<TestClass>(TestClass);
    expect(testClass.persistTickGateway).to.not.equal(null);
  });
});
