import { JsonProperty } from "json-typescript-mapper";
import { IQuote } from "./../../entity-boundary/IQuote";

export class RobinhoodQuote implements IQuote {
  @JsonProperty("ask_price")
  readonly askPrice: string;
  @JsonProperty("ask_size")
  readonly askSize: number;
  @JsonProperty("bid_price")
  readonly bidPrice: string;
  @JsonProperty("bid_size")
  readonly bidSize: string;
  @JsonProperty("last_trade_price")
  readonly lastTradePrice: string;
  @JsonProperty("last_extended_hours_trade_price")
  readonly lastExtendedHoursTradePrice: string;
  @JsonProperty("previous_close")
  readonly previousClose: string;
  @JsonProperty("adjusted_previous_close")
  readonly adjustedPreviousClose: string;
  @JsonProperty("previous_close_date")
  readonly previousCloseDate: string;
  @JsonProperty("symbol")
  readonly symbol: string;
  @JsonProperty("trading_halted")
  readonly tradingHalted: boolean;
  @JsonProperty("has_traded")
  readonly hasTraded: boolean;
  @JsonProperty("last_trade_price_source")
  readonly lastTradePriceSource: string;
  @JsonProperty("updated_at")
  readonly updatedAt: string;
  @JsonProperty("instrument")
  readonly instrument: string;
}
