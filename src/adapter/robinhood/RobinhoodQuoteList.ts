import { JsonProperty } from "json-typescript-mapper";
import { IQuote } from "../../entity-boundary/IQuote";

export class RobinhoodQuoteList {
  @JsonProperty("results")
  readonly results: RobinhoodQuote[];
}
