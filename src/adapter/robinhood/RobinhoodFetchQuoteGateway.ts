import { inject, injectable } from "inversify";
import { deserialize } from "json-typescript-mapper";
import "reflect-metadata";
import { IFetchQuoteGateway } from "../../entity-boundary/IFetchQuoteGateway";
import { TYPE } from "../../types/types";
import { IQuoteData } from "./IQuoteData";
import { RobinhoodQuote } from "./RobinhoodQuote";
import { RobinhoodTickerSymbol } from "./RobinhoodTickerSymbol";

@injectable()
export class RobinhoodFetchQuoteGateway implements IFetchQuoteGateway {
  @inject(TYPE.IQuoteData)
  private readonly quoteData: IQuoteData;

  fetch(robinhoodTickerSymbol: RobinhoodTickerSymbol): Promise<RobinhoodQuote> {
    return this.quoteData.quote(robinhoodTickerSymbol);
  }
}
