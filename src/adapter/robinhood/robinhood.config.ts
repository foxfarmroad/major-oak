import { Container } from "inversify";
import { RobinhoodNode } from "robinhood-node";
import { TYPE } from "../../types/types";

const RobinhoodContainer = new Container();

RobinhoodContainer.bind<RobinhoodNode>(TYPE.RobinhoodNode).toConstantValue(new RobinhoodNode());

export { RobinhoodContainer };
