import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinonChai from "sinon-chai";

chai.use(sinonChai);
chai.use(chaiAsPromised);

const expect = chai.expect;
const assert = chai.assert;
const should = chai.should();

import "reflect-metadata";
import { TYPE } from "../../types/types";
import { container } from "./dynamodb.config.test";
import { DynamoDBTick } from "./DynamoDBTick";
import { IDataMapper } from "./IDataMapper";

describe("RobinhoodQuoteData", () => {
  let testTick;

  beforeEach(() => {
    testTick = new DynamoDBTick();
    testTick.symbol = "TEST";
    testTick.timestamp = Date.now();
    testTick.price = 1.2;
    testTick.volume = 1234567890;
  });

  it("can be instantiated", () => {
    const dataMapper: IDataMapper = container.get<IDataMapper>(TYPE.IDataMapper);
    expect(dataMapper).to.not.equal(null);
  });

  it("can put", async () => {
    const dataMapper: IDataMapper = container.get<IDataMapper>(TYPE.IDataMapper);
    const result = await dataMapper.put(testTick);
    assert.deepEqual(result, testTick);
  });
});
