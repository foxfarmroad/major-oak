import { RobinhoodQuote } from "./RobinhoodQuote";
import { RobinhoodTickerSymbol } from "./RobinhoodTickerSymbol";

export interface IQuoteData {
  quote(robinhoodTickerSymbol: RobinhoodTickerSymbol): Promise<RobinhoodQuote>;
}
