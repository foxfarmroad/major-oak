import { inject, injectable } from "inversify";
import { deserialize } from "json-typescript-mapper";
import { RobinhoodNode } from "robinhood-node";
import { TYPE } from "../../types/types";
import { IQuoteData } from "./IQuoteData";
import { RobinhoodQuote } from "./RobinhoodQuote";
import { RobinhoodQuoteList } from "./RobinhoodQuoteList";
import { RobinhoodTickerSymbol } from "./RobinhoodTickerSymbol";

@injectable()
export class RobinhoodQuoteData implements IQuoteData {
  @inject(TYPE.RobinhoodNode)
  private readonly robinhoodNode: RobinhoodNode;

  quote(robinhoodTickerSymbol: RobinhoodTickerSymbol): Promise<RobinhoodQuote> {
    return new Promise((resolve, reject) => {
      const stock: Stock = new this.robinhoodNode.Stock(robinhoodTickerSymbol.symbol);
      const quoteList: RobinhoodQuoteList[] = deserialize(RobinhoodQuoteList, await stock.quote());
      if (quoteList && quoteList.length > 0) {
        resolve(quoteList[0]);
      } else {
        reject(new Error(`Unable to resolve quote data for ${robinhoodTickerSymbol}`));
      }
    });
  }
}
