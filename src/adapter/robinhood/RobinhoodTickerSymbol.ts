import { ITickerSymbol } from "../../entity-boundary/ITickerSymbol";

export class RobinhoodTickerSymbol implements ITickerSymbol {
  constructor(readonly symbol: string) {}
}
