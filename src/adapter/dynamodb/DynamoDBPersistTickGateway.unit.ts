import { Substitute } from "@fluffy-spoon/substitute";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinonChai from "sinon-chai";
import { ITick } from "../../entity-boundary/ITick";
import { IPersistGateway } from "./../../entity-boundary/IPersistGateway";

chai.use(sinonChai);
chai.use(chaiAsPromised);

const expect = chai.expect;
const assert = chai.assert;

import { Container } from "inversify";
import "reflect-metadata";
import { IPersistTickGateway } from "../../entity-boundary/IPersistTickGateway";
import { TYPE } from "../../types/types";
import { DynamoDBContainer } from "./dynamodb.config";
import { DynamoDBPersistTickGateway } from "./DynamoDBPersistTickGateway";
import { IDataMapper } from "./IDataMapper";

class TestTick implements ITick {
  readonly symbol = "TEST";
  readonly timestamp = 0;
  readonly price: 1;
  readonly volume: 2;
}

describe("DynamoDBPersistTickGateway", () => {
  let testTick;

  const dynamoDbPersistTickGatewayContainer = new Container();
  dynamoDbPersistTickGatewayContainer.bind<IPersistTickGateway>(TYPE.IPersistTickGateway)
      .to(DynamoDBPersistTickGateway);

  const testContainer = Container.merge(DynamoDBContainer, dynamoDbPersistTickGatewayContainer);

  beforeEach(() => {
    testContainer.snapshot();
    testTick = new TestTick();
  });

  afterEach(() => {
    testContainer.restore();
  });

  it("can persist", async () => {
    testContainer.unbind(TYPE.IDataMapper);
    const mockedDataMapper = Substitute.for<IDataMapper>();
    mockedDataMapper.put(testTick).returns(new Promise((resolve, reject) => {
      resolve(testTick);
    }));
    testContainer.bind<IDataMapper>(TYPE.IDataMapper).toConstantValue(mockedDataMapper);

    const persistTickGateway = testContainer.get<IPersistTickGateway>(TYPE.IPersistTickGateway);
    const result = await persistTickGateway.persist(testTick);
    assert.deepEqual(result, testTick);
    mockedDataMapper.received(1).put(testTick);
  });

  it("can error", async () => {
    testContainer.unbind(TYPE.IDataMapper);
    const mockedDataMapper = Substitute.for<IDataMapper>();
    mockedDataMapper.put(testTick).returns(new Promise((resolve, reject) => {
      reject(new Error());
    }));
    testContainer.bind<IDataMapper>(TYPE.IDataMapper).toConstantValue(mockedDataMapper);

    const persistTickGateway = testContainer.get<IPersistTickGateway>(TYPE.IPersistTickGateway);
    assert.isRejected(persistTickGateway.persist(testTick));
    mockedDataMapper.received(1).put(testTick);
  });
});
