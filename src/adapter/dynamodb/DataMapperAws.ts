import { DataMapper } from "@aws/dynamodb-data-mapper";
import { inject, injectable } from "inversify";
import { TYPE } from "../../types/types";
import { IDataMapper } from "./IDataMapper";

import { DynamoDBTick } from "./DynamoDBTick";

@injectable()
export class DataMapperAws implements IDataMapper {
  @inject(TYPE.DataMapper)
  private readonly dataMapper: DataMapper;

  put(item: DynamoDBTick): Promise<DynamoDBTick> {
    return this.dataMapper.put(item);
  }
}
