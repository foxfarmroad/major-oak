import { DataMapper } from "@aws/dynamodb-data-mapper";
import DynamoDB = require("aws-sdk/clients/dynamodb");
import { Container } from "inversify";
import { IPersistTickGateway } from "../../entity-boundary/IPersistTickGateway";
import { TYPE } from "../../types/types";
import { DataMapperAws } from "./DataMapperAws";
import { DynamoDBPersistTickGateway } from "./DynamoDBPersistTickGateway";
import { IDataMapper } from "./IDataMapper";

const DynamoDBContainer = new Container();

DynamoDBContainer.bind<DataMapper>(TYPE.DataMapper).toConstantValue(
  new DataMapper(
    {
      client: new DynamoDB(
        {
          region: process.env.REGION ? process.env.REGION : "us-west-2",
        },
      ),
    },
  ),
);
DynamoDBContainer.bind<IDataMapper>(TYPE.IDataMapper).to(DataMapperAws);

export { DynamoDBContainer };
