import { inject, injectable } from "inversify";
import "reflect-metadata";
import { IPersistTickGateway } from "../../entity-boundary/IPersistTickGateway";
import { TYPE } from "../../types/types";
import { DynamoDBTick } from "./DynamoDBTick";
import { IDataMapper } from "./IDataMapper";

@injectable()
export class DynamoDBPersistTickGateway implements IPersistTickGateway {

  @inject(TYPE.IDataMapper)
  private readonly dataMapper: IDataMapper;

  persist(tick: DynamoDBTick): Promise<DynamoDBTick> {
    return this.dataMapper.put(tick);
  }
}
