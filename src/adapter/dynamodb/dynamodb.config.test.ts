import { DataMapper } from "@aws/dynamodb-data-mapper";
import * as AWS from "aws-sdk";
import DynamoDB = require("aws-sdk/clients/dynamodb");
import { Container } from "inversify";
import { IPersistTickGateway } from "../../entity-boundary/IPersistTickGateway";
import { TYPE } from "../../types/types";
import { DataMapperAws } from "./DataMapperAws";
import { DynamoDBPersistTickGateway } from "./DynamoDBPersistTickGateway";
import { IDataMapper } from "./IDataMapper";

AWS.config.credentials = new AWS.SharedIniFileCredentials({profile: "major-oak-test"});

const container = new Container();

container.bind<DataMapper>(TYPE.DataMapper).toConstantValue(
  new DataMapper(
    {
      client: new DynamoDB(
        {
          region: process.env.REGION ? process.env.REGION : "us-west-2",
        },
      ),
    },
  ),
);
container.bind<IDataMapper>(TYPE.IDataMapper).to(DataMapperAws);
container.bind<IPersistTickGateway>(TYPE.IPersistTickGateway).to(DynamoDBPersistTickGateway);

export { container };
