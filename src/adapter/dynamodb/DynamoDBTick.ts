import {
  attribute,
  hashKey,
  rangeKey,
  table,
} from "@aws/dynamodb-data-mapper-annotations";
import { ITick } from "./../../entity-boundary/ITick";

@table("sherwood-stock-history-journal-table-dev")
export class DynamoDBTick implements ITick {
  @hashKey()
  symbol: string;
  @rangeKey()
  timestamp: number;
  @attribute()
  price: number;
  @attribute()
  volume: number;
}
