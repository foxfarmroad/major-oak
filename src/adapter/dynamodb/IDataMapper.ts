import { DynamoDBTick } from "./DynamoDBTick";

export interface IDataMapper {
  put(item: DynamoDBTick): Promise<DynamoDBTick>;
}
