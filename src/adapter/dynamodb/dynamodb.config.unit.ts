import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as sinonChai from "sinon-chai";

chai.use(sinonChai);
chai.use(chaiAsPromised);

const expect = chai.expect;

import { DataMapper } from "@aws/dynamodb-data-mapper";
import "reflect-metadata";
import { TYPE } from "../../types/types";
import { DynamoDBContainer } from "./dynamodb.config";
import { IDataMapper } from "./IDataMapper";

import { inject, injectable } from "inversify";

@injectable()
class TestClass {
  @inject(TYPE.DataMapper)
  readonly dataMapper: DataMapper;
  @inject(TYPE.IDataMapper)
  readonly idataMapper: IDataMapper;
}

describe("DynamoDB config", () => {
  it("injects dependencies", () => {
    const testClass: TestClass = DynamoDBContainer.resolve<TestClass>(TestClass);
    expect(testClass.dataMapper).to.not.equal(null);
    expect(testClass.idataMapper).to.not.equal(null);
  });
});
