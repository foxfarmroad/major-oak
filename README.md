TODO: Proper README

Some notes on creating the project and how it is structured

First attempt creating a TypeScript project written using Clean Architecture

npm init

Clean Architecture

Entity

Objects

Tick

Tick.prototype = Object.create(TickInterface.prototype)
Tick.prototype.getSymbol = function() { return this.symbol; }
Tick.prototype.getTimestamp = function() { return this.timestamp; }
Tick.prototype.getValue = function() { return this.value; }
Tick.prototype.getVolume = function() { return this.volume; }
…
// same for set

Entity Boundary

Interfaces

ITickGateway

Use Case

Interfaces

TickGateway



persistTick(Tick);
Adapter
persistTick(Tick) { // create DynamoDB record }

aws dynamodb put-item --table-name sherwood-stock-history-journal-table-dev --item '{"symbol": {"S": "TEST"}, "timestamp": {"S": "1537388360000"}}' --profile major-oak-test